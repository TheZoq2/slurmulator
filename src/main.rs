use anyhow::{anyhow, Context, Result};
use lazy_static::lazy_static;
use log::{error, info, warn, LevelFilter};
use regex::Regex;
use std::{
    path::PathBuf,
    process::Command,
    sync::{Arc, Mutex},
};

use simplelog::{Config, TermLogger, TerminalMode};
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    /// Number of threads to run in parallel
    #[structopt(short = "n", long = "num-threads")]
    num_threads: u32,
    /// Script to execute
    #[structopt()]
    script: PathBuf,
}
fn main() -> Result<()> {
    TermLogger::init(LevelFilter::Info, Config::default(), TerminalMode::Mixed)
        .expect("Failed to initialise logger");

    let opts = Opt::from_args();

    // Parse the script for sbatch variables
    let script_content =
        String::from_utf8(std::fs::read(&opts.script).context("Failed to open script")?)
            .context("Failed to parse script as utf-8")?;

    let mut array_bounds = None;
    for (i, line) in script_content.lines().enumerate() {
        // We only allow shebangs, empty lines and sbatch comments before giving up
        // on parsing
        if line.starts_with("#!") || line.is_empty() {
            continue;
        } else if line.starts_with("#SBATCH") {
            // Do the actual parsing
            lazy_static! {
                static ref ARRAY_RE: Regex = Regex::new(r#"#SBATCH --array=(\d+)-(\d+)"#).unwrap();
                static ref N_RE: Regex = Regex::new(r#"SBATCH -n\d+"#).unwrap();
            };

            if let Some(captures) = ARRAY_RE.captures(line) {
                // NOTE safe unwraps. Regex only matches digits which must exist for a match
                // and which are parsable as ints
                let min_array = captures.get(1).unwrap().as_str().parse::<u32>().unwrap();
                let max_array = captures.get(2).unwrap().as_str().parse::<u32>().unwrap();

                array_bounds = Some((min_array, max_array));
            } else if N_RE.is_match(line) {
                warn!("Line: {}: Unhandled SBATCH flag -n", line);
            } else {
                return Err(anyhow!("Line: {}: Unrecognised sbatch flag `{}`", i, line));
            }
        } else if line.starts_with("# SBATCH") {
            warn!(
                "Line: {}: SBATCH flags must not have a space between # and SBATCH",
                i
            )
        } else {
            // We reached non-configuration things, break out of the loop
            break;
        }
    }

    let (min_array, max_array) = if let Some((min, max)) = array_bounds {
        (min, max)
    } else {
        return Err(anyhow!("Sbatch script does not specify an array parameter"));
    };

    info!(
        "Running with array {}-{} with {} threads",
        min_array, max_array, opts.num_threads
    );

    let job_list = Arc::new(Mutex::new((min_array..max_array).collect::<Vec<_>>()));

    let mut handles = vec![];
    for thread_id in 0..opts.num_threads {
        let list = job_list.clone();
        let script = opts.script.clone();
        handles.push(std::thread::spawn(move || {
            // This closure is needed because the lock needs to go out of scope as soon as
            // possible, but just calling .lock().unwrap() leaves the mutex locked for the duration
            // of the while loop, preventing any threads from working
            while let Some(task) = (|| {list.lock().unwrap().pop()})() {
                info!("Thread {} is running task {}", thread_id, task);
                let output = Command::new(&script)
                    .env("SLURM_ARRAY_TASK_ID", format!("{}", task))
                    .output()
                    .expect("Failed to launch command");

                std::fs::write(&format!("task_{}.stderr", task), &output.stderr)
                    .expect(&format!("failed to write stderr of task {}", task));
                std::fs::write(&format!("task_{}.stdout", task), &output.stdout)
                    .expect(&format!("failed to write stdout of task {}", task));

                if let Some(code) = output.status.code() {
                    if code != 0 {
                        error!("Task {} failed with code {}", task, code)
                    }
                }
            }
        }))
    }

    for (i, handle) in handles.into_iter().enumerate() {
        handle
            .join()
            .expect(&format!("Failed to join thread {}", i));
    }

    Ok(())
}
